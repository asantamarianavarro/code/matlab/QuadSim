% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% PX4Flow_PARAMS
%
% Script that defines all PX4Flow parameters required y PX4Flow simulink block. 
% It increments the variables params.
% Initially (all 0's) it is considered mounted below the quadrotor and facing downwards

% Range
params.px4flow.std.r = 5e-4; % Range measurement std dev

% GYROSCOPES
params.px4flow.std.w = 1e-5*ones(3,1); % Gyroscopes std dev.
params.px4flow.std.wb = 0e-6*ones(3,1); % Bias random walk std dev.

% Velocity XY
params.px4flow.std.velxy = 1e-3*ones(2,1); % Vxy std dev

% Flow 2D
params.px4flow.std.flow2d = 20*ones(2,1); % Flow2D measurement std dev. (pixels)
params.px4flow.f = 500; % Focal length in pixels (See PX4 online datasheet).

% Extrinsic parameters
params.px4flow.pose = [0.0 0.0 0.0 0.0 0.0 0.0]'; % [x,y,z,yaw,pitch,roll]

% Frequency
params.px4flow.freq = 100; % Hz