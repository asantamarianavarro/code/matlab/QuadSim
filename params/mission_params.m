% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.


%% Mission_params 
% 
% Contains the mission actions to be executed by the quadrotor
% The type of actions include:
%    - take off (toff)
%    - land (land)
%    - waypoints (wp): [x;y;z;yaw] column vector. Frame: North West Up (XYZ)
%    - err: min error to consider wp reached (radius of the sphere). m.
%         - lin: Linear error.
%         - ang: Angular error.
%    - wait: time waiting in wp. s.

% Example Mission ___________

trajnum = 1;

switch trajnum

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    case 0
        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [350.0;350.0;3.5;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [0.0;0.0;1.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
    
        actions(4).type = 'land';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    case 1

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;0.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;10.0;3.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-10.0;3.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';        
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    case 2
        
        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;0.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 3

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;-10.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;0.0;3.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [-10.0;10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-10.0;3.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;3.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    case 4

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;-10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [-10.0;10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
         
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 5

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;0.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;-10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;0.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 6

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;0.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;-10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;0.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;3.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 7

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;0.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;-10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;0.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 8

       actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;0.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [0.0;-10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;0.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 9

       actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 10

       actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;10.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
         
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 11

       actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;0.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;10.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;-10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;0.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
         
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 12

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [10.0;0.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;3.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;-10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;0.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';      
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 13

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [-10.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 14

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [10.0;10.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [10.0;10.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [-10.0;-10.0;3.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';                 
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 15


        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;0.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 16

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;10.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;0.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
                
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 17

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;0.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
                
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 18

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [0.0;0.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;10.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [0.0;0.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;0.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [0.0;0.0;3.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [0.0;-10.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;0.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
                
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 19

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;5.0;2.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;5.0;2.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [5.0;10.0;2.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [5.0;10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-5.0;2.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;2.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
                
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
    case 20

        actions(1).type = 'toff';
        
        actions(2).type = 'wp';
        actions(2).wp.data = [10.0;5.0;3.0;0.0];
        actions(2).wp.wait = 1.0;
        actions(2).wp.err.lin = 0.1;
        actions(2).wp.err.ang = 0.2;
        
        actions(3).type = 'wp';
        actions(3).wp.data = [-10.0;5.0;3.0;0.0];
        actions(3).wp.wait = 1.0;
        actions(3).wp.err.lin = 0.1;
        actions(3).wp.err.ang = 0.2;
        
        actions(4).type = 'wp';
        actions(4).wp.data = [5.0;10.0;3.0;0.0];
        actions(4).wp.wait = 1.0;
        actions(4).wp.err.lin = 0.1;
        actions(4).wp.err.ang = 0.2;
        
        actions(5).type = 'wp';
        actions(5).wp.data = [-10.0;10.0;2.0;0.0];
        actions(5).wp.wait = 1.0;
        actions(5).wp.err.lin = 0.1;
        actions(5).wp.err.ang = 0.2;
        
        actions(6).type = 'wp';
        actions(6).wp.data = [5.0;10.0;2.0;0.0];
        actions(6).wp.wait = 1.0;
        actions(6).wp.err.lin = 0.1;
        actions(6).wp.err.ang = 0.2;
        
        actions(7).type = 'wp';
        actions(7).wp.data = [10.0;-5.0;3.0;0.0];
        actions(7).wp.wait = 1.0;
        actions(7).wp.err.lin = 0.1;
        actions(7).wp.err.ang = 0.2;
        
        actions(8).type = 'wp';
        actions(8).wp.data = [10.0;10.0;3.0;0.0];
        actions(8).wp.wait = 1.0;
        actions(8).wp.err.lin = 0.1;
        actions(8).wp.err.ang = 0.2;
        
        actions(9).type = 'wp';
        actions(9).wp.data = [0.0;0.0;2.0;0.0];
        actions(9).wp.wait = 1.0;
        actions(9).wp.err.lin = 0.1;
        actions(9).wp.err.ang = 0.2;
        
        actions(10).type = 'land';          
         
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
end
