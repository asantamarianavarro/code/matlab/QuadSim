% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [hobj] = drawLine(parent,points,color,style)
%   _____________________
% 
%   Generate a Line handler.
%
%   [hobj] = drawLine(parent,points,color,style)
%
%   Inputs:
%       - parent:   Axes handler where the quadrotor will be plotted. 
%       - points:   Line Points.
%       - color:    Line Color.
%       - style:    Line Style.
%
%   Ouputs:
%       - hx:       X axis object handler.
%       - posx:     Positions of the X axis vertices.
%       - hy:       Y axis object handler.
%       - posy:     Positions of the Y axis vertices.
%       - hz:       Z axis object handler.
%       - posz:     Positions of the Z axis vertices.
%
%   _____________________

switch nargin
    case 2
        color = 'k';
        style = '-';
    case 3
        style = '-';
end

hobj = line(...
    'parent', parent,...
    'color',color,...
    'linestyle',style,...
    'xdata',points(1,:),...
    'ydata',points(2,:),...
    'zdata',points(3,:),...
    'LineWidth',1);

return