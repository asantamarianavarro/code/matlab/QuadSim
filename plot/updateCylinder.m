% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [] = updateCylinder(hCyl0,hCyl,pos,R)
%   _____________________
% 
%   Update Cylinder Handler
%
%   [] = updateCylinder(hCyl0,hCyl,pos,R)
%
%   Inputs:
%       - hCyl0:   Generc Cylinder handler at 0,0 to easy the obtantion of points.
%       - hCyl:    Cylinder handler to be updated.
%       - pos:     New position for Cylinder handler.
%       - R:       New Rotation for Cylinder handler. 
%   _____________________

Cxyz = get_points(hCyl0(1));
Cep1xyz = get_points(hCyl0(2)); 
Cep2xyz = get_points(hCyl0(3));

Cxyz_new = R*Cxyz + repmat(pos,1,size(Cxyz,2));
Cep1xyz_new = R*Cep1xyz + repmat(pos,1,size(Cep1xyz,2));
Cep2xyz_new = R*Cep2xyz + repmat(pos,1,size(Cep2xyz,2));

set(hCyl(1),'XData',[Cxyz_new(1,1:size(Cxyz_new,2)/2)' Cxyz_new(1,(size(Cxyz_new,2)/2)+1:size(Cxyz_new,2))'],'YData',[Cxyz_new(2,1:size(Cxyz_new,2)/2)' Cxyz_new(2,(size(Cxyz_new,2)/2)+1:size(Cxyz_new,2))'],'ZData',[Cxyz_new(3,1:size(Cxyz_new,2)/2)' Cxyz_new(3,(size(Cxyz_new,2)/2)+1:size(Cxyz_new,2))']);
set(hCyl(2),'XData',Cep1xyz_new(1,:),'YData',Cep1xyz_new(2,:),'ZData',Cep1xyz_new(3,:));
set(hCyl(3),'XData',Cep2xyz_new(1,:),'YData',Cep2xyz_new(2,:),'ZData',Cep2xyz_new(3,:));

return