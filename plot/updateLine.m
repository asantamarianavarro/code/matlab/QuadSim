% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [points_new] = updateLine(hobj,point)
%   _____________________
% 
%   Update Line handler and points
%
%   [points_new] = updateLine(hobj,points,point)
%
%   Inputs:
%       - hobj:         Object handler
%       - point:        New point to be added
%
%   Outputs:
%       - points_new    New set of points with the update point
%
%   _____________________

points = get_points(hobj);

points_new = [points point];

set(hobj,'XData',points_new(1,:),'YData',points_new(2,:),'ZData',points_new(3,:));

hold on

return
