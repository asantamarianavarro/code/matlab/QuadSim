% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% Quadrotor Plot
% 
% Creates and updates all necessary handlers to plot a 3D scene with a
% quadrotor moving around.

function [sys,x0,str,ts] = quad_plot(t,x,u,flag,enable,plot)
    % This S-Function plots and updates a Quadrotro 3D plot
        
    % INPUTS
    % 1 Center X position
    % 2 Center Y position
    % 3 Center Z position
    % 4 Yaw angle in rad
    % 5 Pitch angle in rad
    % 6 Roll angle in rad
    
    % OUTPUTS
    %   None   
    ts = [-1 0];
    
    switch flag,
        case 0
            [sys,x0,str,ts] = mdlInitializeSizes(ts,enable,plot); % Initialization
        case 3
            sys = mdlOutputs(t,u,enable,plot); % Calculate outputs
        case {1,2, 4, 9} % Unused flags
            sys = [];
        otherwise
            error(['unhandled flag = ',num2str(flag)]); % Error handling
    end
    
    
    % Initialize
function [sys,x0,str,ts] = mdlInitializeSizes(ts,enable,plot)

global plot_hdl quad_initialized current_wp

  % Call simsizes for a sizes structure, fill it in, and convert it
  % to a sizes array.
  sizes = simsizes;
  sizes.NumContStates  = 0;
  sizes.NumDiscStates  = 0;
  sizes.NumOutputs     = 0;
  sizes.NumInputs      = 11;
  sizes.DirFeedthrough = 1;
  sizes.NumSampleTimes = 1;
  sys = simsizes(sizes);
  x0  = [];
  str = [];          % Set str to an empty matrix.
  ts = [0.05 0]; % 20 fps
    
  if enable == 1

    % Main figure handler
    plot_hdl.fig = figure(99);
    set(plot_hdl.fig,'NumberTitle','off','Name','Quadrotor simulation','Renderer','OpenGL','Position', [300, 300, 640, 480]);
    
    % Axis handler
    plot_hdl.ax = axes('Parent',plot_hdl.fig);
    axis equal

    % plot view    
    az = plot.view.az;
    el = plot.view.el;
    view(plot_hdl.ax,az,el)
    set(gcf,'Color',[0.9,0.9,0.9])
    set(gca,'FontSize',plot.font_size,'box','off','layer','top','TickDir','out')
    xlab = 'x (m)';
    ylab = 'y (m)';
    zlab = 'z (m)';
    xlabel(xlab,'interpreter','latex','FontSize',plot.font_size);
    ylabel(ylab,'interpreter','latex','FontSize',plot.font_size);
    zlabel(zlab,'interpreter','latex','FontSize',plot.font_size);
    axis(plot_hdl.ax,plot.scene_size);
    set(findall(gcf,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
    camzoom(1.3);
    
    % Main handlers: Draw functions    
    plot_hdl.floor = drawFloor(plot_hdl.ax); %Floor reference
    plot_hdl.frames.world = drawFrame(plot_hdl.ax,plot.scene_center,plot.frame_length); %World frame
    plot_hdl.robot.quad_gen = drawRobot(plot_hdl.ax,plot.scene_center,plot,'off'); %Generic Robot 
    quad_initialized = false;
    
    % Current WP initialization
    current_wp = zeros(5,1); % [enabled;x;y;z;tolerance]
  end
  % End of mdlInitializeSizes.
    
    
function sys = mdlOutputs(t,u,enable,plot)
  
  global plot_hdl quad_initialized current_wp
 
  %READ STATE
  state = [u(1);u(2);u(3);u(4);u(5);u(6)]; % [x;y;z;-yaw;-pitch;-roll] % Local to Global angles.
  
  %READ WP info
  wp_info = [u(7);u(8);u(9);u(10);u(11)]; % Desired WP info: [wp_active;x;y;z;wp_tolerance]

  if enable == 1      
      if quad_initialized
          updateRobot(state,plot_hdl.robot.quad_gen,plot_hdl.robot.quad.hndl);
          updateLine(plot_hdl.robot.quad.odom,state(1:3,1));
          updateFloor(plot_hdl.ax,plot_hdl.floor);
          updateAxis(plot_hdl.ax,state);
          drawnow  
      else
          plot_hdl.robot.quad.hndl = drawRobot(plot_hdl.ax,state(1:3,1),plot,'on'); %Robot
          plot_hdl.robot.quad.odom = drawLine(plot_hdl.ax,state(1:3,1),plot.robot.quad.params.odom.color,plot.robot.quad.params.odom.style); %Odometry line
          quad_initialized = true;
      end          
  end
  
  % Plot WP if necessary
  if wp_info(1,1) == 1
      if ~isequal(wp_info(2:5),current_wp(2:5))
          if current_wp(1,1) == 1
              updateWP(plot_hdl.wp.hndl,[0.8 0.8 0.8],'on');
              current_wp = wp_info;
              plot_hdl.wp.hndl = drawWP(plot_hdl.ax,current_wp(2:5)); % WayPoint
              updateAxis(plot_hdl.ax,current_wp(2:5));
          else
              current_wp = wp_info;
              plot_hdl.wp.hndl = drawWP(plot_hdl.ax,current_wp(2:5)); % WayPoint
              updateAxis(plot_hdl.ax,current_wp(2:5));
          end
      end    
  else
      if current_wp(1,1)==1
          updateWP(plot_hdl.wp.hndl,[0.8 0.8 0.8],'on');
      end
      current_wp = zeros(5,1);      
  end
  
  sys = [];
  % End of mdlOutputs.
  
