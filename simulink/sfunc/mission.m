% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% Mission
% 
% This file read the mission_params file and sends the corresponding
% commands to the quadrotor controller.

function [sys,x0,str,ts] = mission(t,x,u,flag,enable,actions)
    ts = [-1 0];
    
    switch flag,
        case 0
            [sys,x0,str,ts] = mdlInitializeSizes(ts); % Initialization
        case 3
            sys = mdlOutputs(t,u,enable,actions); % Calculate outputs
        case {1,2, 4, 9} % Unused flags
            sys = [];
        otherwise
            error(['unhandled flag = ',num2str(flag)]); % Error handling
    end
    
    
    % Initialize
function [sys,x0,str,ts] = mdlInitializeSizes(ts)

  global finished current_action

  % Call simsizes for a sizes structure, fill it in, and convert it
  % to a sizes array.
  sizes = simsizes;
  sizes.NumContStates  = 0;
  sizes.NumDiscStates  = 0;
  sizes.NumOutputs     = 10;
  sizes.NumInputs      = 4;
  sizes.DirFeedthrough = 1;
  sizes.NumSampleTimes = 1;
  sys = simsizes(sizes);
  x0  = [];
  str = [];          % Set str to an empty matrix.
  ts = [0.01 0];
  
  finished = true;
  current_action = 0;
  
  % End of mdlInitializeSizes.
    
    
function sys = mdlOutputs(t,u,enable,actions)
  
  global current_action finished wp_arrival_time
  
  %READ STATE
  state = [u(1);u(2);u(3);u(4)];
 
  cmd = zeros(4,1); % Ctrl Command: Desired WP: [x;y;z;yaw]
  wp_info = zeros(5,1); % Desired WP info: [wp_active;x;y;z;wp_tolerance]
    
  if enable
      if finished && size(actions,2)-current_action > 0
          current_action = 1;
          finished = false;
          disp(['Running: ' actions(current_action).type])
      else
          if t > 1.0  % wait 1s to stabilize all modules 
          if current_action <= size(actions,2)
              if strcmp(actions(current_action).type,'toff')
                  des_height = 0.5;
                  cmd = state; % Proportional Ctrl here suffices
                  cmd(3) = des_height;
                  err = des_height - state(3); % Hover at 1m
                  if abs(err) < 0.01 
                      current_action = current_action + 1;  
                      if current_action <= size(actions,2)
                          disp(['Running: ' actions(current_action).type])
                      else
                          finished = true;
                          disp('Mission complete.');
                      end
                  end
              elseif strcmp(actions(current_action).type,'land')
                  des_height = 0.0;
                  cmd = state; % Proportional Ctrl here suffices
                  cmd(3) = des_height;
                  err = des_height - state(3); % Hover at 1m
                  if abs(err) < 0.01 
                      current_action = current_action + 1;  
                      if current_action <= size(actions,2)
                          disp(['Running: ' actions(current_action).type])
                      else
                          finished = true;
                          disp('Mission complete.');
                      end
                  end
              elseif strcmp(actions(current_action).type,'wp')
                  des = actions(current_action).wp.data;
                  wp_info = [1;des(1:3,1);actions(current_action).wp.err.lin];
                  cmd = des; % Proportional Ctrl here suffices
                  err = des - state;
                  if norm(err(1:3)) < actions(current_action).wp.err.lin && abs(err(4)) < actions(current_action).wp.err.ang
                      if wp_arrival_time == 0.0
                          wp_arrival_time = get_param('quadrotor','SimulationTime');
                      else
                          waiting_time = get_param('quadrotor','SimulationTime') - wp_arrival_time;
                          if waiting_time > actions(2).wp.wait
                              disp('Waypoint done.')
                              current_action = current_action + 1;  
                              if current_action <= size(actions,2)
                                  disp(['Running: ' actions(current_action).type])
                              else
                                  finished = true;
                                  disp('Mission complete.');
                              end
                          end
                      end
                  else
                      wp_arrival_time = 0.0;
                  end
              end
          end
          end
      end
      if finished
          cmd = state; 
      end
  end
  sys = [cmd;wp_info;finished]; % x,y,z,yaw     
  % End of mdlOutputs.
  